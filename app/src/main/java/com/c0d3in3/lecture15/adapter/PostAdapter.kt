package com.c0d3in3.lecture15.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.c0d3in3.lecture15.R
import com.c0d3in3.lecture15.model.PostModel
import kotlinx.android.synthetic.main.image_item_layout.view.*
import kotlinx.android.synthetic.main.post_item_layout.view.*

class PostAdapter(private val posts : ArrayList<PostModel>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object{
        const val POST_ID = 1
        const val IMAGE_ID = 2
    }

    override fun getItemCount(): Int = posts.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) : RecyclerView.ViewHolder = if(viewType == POST_ID) PostViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.post_item_layout, parent, false))
        else ImageViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.image_item_layout, parent, false))

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if(holder is PostViewHolder){
            holder.onBind()
        }
        else if(holder is ImageViewHolder){
            holder.onBind()
        }
    }

    inner class PostViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){
        fun onBind(){
            itemView.postTitleTextView.text = posts[adapterPosition].title
            itemView.descriptionTextView.text = posts[adapterPosition].description!!
        }
    }

    inner class ImageViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){
        fun onBind(){
            itemView.titleTextView.text = posts[adapterPosition].title
            itemView.imageView.setImageResource(posts[adapterPosition].Image!!)
        }
    }

    override fun getItemViewType(position: Int) : Int = if(posts[position].type == 1) POST_ID
        else IMAGE_ID

}