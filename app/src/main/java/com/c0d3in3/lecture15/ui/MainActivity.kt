package com.c0d3in3.lecture15.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import androidx.recyclerview.widget.GridLayoutManager
import com.c0d3in3.lecture15.R
import com.c0d3in3.lecture15.adapter.PostAdapter
import com.c0d3in3.lecture15.model.PostModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val posts = arrayListOf<PostModel>() // initialize arraylist of posts
    private val adapter = PostAdapter(posts) // initialize adapter with posts arraylist
    private val swipeHandler = Handler() // initialize handler for swipe refresh listener
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setUp()
    }

    private fun setUp(){
        recyclerView.layoutManager = GridLayoutManager(this, 2) // setting recyclerview's layoutmanager to grid
        recyclerView.adapter = adapter

        setData()

        swipeRefreshLayout.setOnRefreshListener {
            swipeRefreshLayout.isRefreshing = true
            posts.clear() // clearing posts array
            adapter.notifyDataSetChanged() // notifying adapter about remove
            swipeHandler.postDelayed({ setData() }, 2000) // setting post with delay
        }
    }

    private fun setData(){
        posts.add(PostModel("d e v i l", null, R.mipmap.vinne_01, 2))
        posts.add(PostModel("c u t e  d e v i l ",null,  R.mipmap.vinne_02, 2))
        posts.add(PostModel("s u m m e r", "my custom description blablabalba", null, 1))
        posts.add(PostModel("s u m m e r", null, R.mipmap.vinne_03, 2))
        posts.add(PostModel("i d k", null, R.mipmap.vinne_04, 2))
        posts.add(PostModel("c h i l l i n g", null, R.mipmap.vinne_05, 2))
        posts.add(PostModel("c a p s u l e",null,  R.mipmap.vinne_06, 2))
        posts.add(PostModel("m y  p o s t", "blablablabla blablabalba",  null, 1))
        posts.add(PostModel("c y b e r  p u n k", null, R.mipmap.vinne_07, 2))
        posts.add(PostModel("w a l k i n g",null, R.mipmap.vinne_08, 2))

        if(swipeRefreshLayout.isRefreshing) swipeRefreshLayout.isRefreshing = false // stop refreshing after data was loaded
        adapter.notifyDataSetChanged()
    }
}
